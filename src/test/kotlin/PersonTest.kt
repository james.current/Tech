import io.kotest.core.spec.style.FreeSpec
import io.kotest.matchers.shouldBe

class PersonTest : FreeSpec({

    "lastName" - {
        "should return trimmed version of lname" {
            val person = Person(
                firstName = "foo",
                lname = " bar ",
                age = 42
            )

            person.lastName() shouldBe "bar"
        }
    }
})
