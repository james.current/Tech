import io.kotest.core.spec.style.FreeSpec
import io.kotest.matchers.shouldBe
import io.mockk.*
import kotlinx.coroutines.channels.Channel

class MainKtTest : FreeSpec({

    "main" - {
        mockkStatic(::fetchFiles)
        mockkStatic(::getPossiblyExpensiveValues)
        mockkStatic(::printReport)

        every { printReport(any(), any(), any(), any()) } just Runs

        val urls = listOf(
            "file:///file1",
            "file:///file2",
            "file:///file3",
            "file:///file4",
        )

        "should handle no data to process" {
            coEvery { fetchFiles(any(), any()) } coAnswers {
                secondArg<Channel<String>>().close()
                FetchFilesResult(listOf(), 42L)
            }

            main(urls.toTypedArray())

            coVerify {
                fetchFiles(
                    withArg {
                        it.size shouldBe urls.size
                    },
                    any()
                )
            }

            verify {
                printReport(
                    null,
                    urls.size,
                    42L,
                    listOf()
                )
            }
        }

        "should handle normal data" {
            val people = listOf(
                Person(
                    firstName = "Jane",
                    lname = "Doe",
                    age = 42
                ),
                Person(
                    firstName = "John",
                    lname = "Doe",
                    age = 42
                )
            )
            val possiblyExpensiveValues = PossiblyExpensiveValues(
                11.0,
                37.0,
                mapOf(19.0 to "Test Name")
            )
            coEvery { fetchFiles(any(), any()) } coAnswers {
                secondArg<Channel<String>>().close()
                FetchFilesResult(people, 42L)
            }
            coEvery { getPossiblyExpensiveValues(any()) } returns possiblyExpensiveValues

            main(urls.toTypedArray())

            coVerify {
                fetchFiles(
                    withArg {
                        it.size shouldBe urls.size
                    },
                    any()
                )
                getPossiblyExpensiveValues(people)
            }

            verify {
                printReport(
                    possiblyExpensiveValues,
                    urls.size,
                    42L,
                    listOf()
                )
            }
        }
    }
})
