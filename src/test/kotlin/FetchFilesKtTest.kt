import io.kotest.core.spec.style.FreeSpec
import io.kotest.matchers.collections.shouldContainExactlyInAnyOrder
import io.kotest.matchers.longs.shouldBeGreaterThan
import io.mockk.*
import kotlinx.coroutines.channels.Channel
import java.io.IOException
import java.net.URL

class FetchFilesKtTest : FreeSpec({

    "fetchFiles" - {
        val people = listOf(
            Person(
                firstName = "Jane",
                lname = "Doe",
                age = 42
            ),
            Person(
                firstName = "John",
                lname = "Doe",
                age = 42
            ),
            Person(
                firstName = "Foo",
                lname = "Bar",
                age = 42
            ),
        )
        val firstFilePeople = people.take(1)
        val firstFileUrl = URL("file:///first")
        val secondFilePeople = people.takeLast(2)
        val secondFileUrl = URL("file:///second")

        val failFileUrl = URL("file:///fail")

        mockkObject(CsvMapperSingleton)
        every { CsvMapperSingleton.getCsv(firstFileUrl) } returns firstFilePeople
        every { CsvMapperSingleton.getCsv(secondFileUrl) } returns secondFilePeople
        every { CsvMapperSingleton.getCsv(failFileUrl) } throws IOException("boom")

        "should handle a complex file setup" {
            val mockChannel = mockk<Channel<String>> {
                coEvery { send(any()) } just Runs
                every { close() } returns true
            }
            val result = fetchFiles(listOf(failFileUrl, firstFileUrl, secondFileUrl), mockChannel)

            result.people shouldContainExactlyInAnyOrder people
            result.clockTimeInMillis shouldBeGreaterThan 0L
            coVerify {
                mockChannel.send(failFileUrl.toString())
            }
            verify {
                mockChannel.close()
            }
        }

        unmockkObject(CsvMapperSingleton)
    }
})
