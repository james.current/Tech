import io.kotest.assertions.throwables.shouldThrowExactly
import io.kotest.core.spec.style.FreeSpec
import io.kotest.matchers.shouldBe
import io.mockk.every
import io.mockk.mockkStatic
import io.mockk.unmockkStatic

class CalculationsKtTest : FreeSpec({

    "getPossiblyExpensiveValues" - {
        mockkStatic(::getMedian)
        val mockedMedian = 42.0
        every { getMedian(any()) } returns mockedMedian

        // simple ages for calculating average
        val testPeople = listOf(
            Person(
                firstName = "John",
                // this is what the data looks like coming in
                lname = " Doe",
                age = 25
            ),
            Person(
                firstName = "Jane",
                lname = " Doe",
                age = 35
            )
        )

        "should return average, median, and age to name map" {
            getPossiblyExpensiveValues(testPeople) shouldBe PossiblyExpensiveValues(
                average = 30.0,
                median = mockedMedian,
                ageToNameMap = mapOf(
                    25.0 to "John Doe",
                    35.0 to "Jane Doe"
                )
            )
        }

        unmockkStatic(::getMedian)
    }

    "getMedian" - {
        "should throw an exception when called with an empty list" {
            shouldThrowExactly<IllegalArgumentException> { getMedian(listOf()) }
        }

        "should handle list size 1" {
            getMedian(listOf(5)) shouldBe 5.0
        }

        "should calculate the median of an odd length list" {
            getMedian(listOf(5, 3, 7, 2, 8)) shouldBe 5.0
        }

        "should calculate the median of an even length list" {
            getMedian(listOf(2, 3, 1, 4)) shouldBe 2.5
        }
    }
})
