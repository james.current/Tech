import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.coroutineScope
import java.io.IOException
import java.net.URL
import kotlin.system.measureTimeMillis

suspend fun fetchFiles(
    fileUrls: List<URL>,
    errorChannel: Channel<String>
) = coroutineScope {
    val people: List<Person>
    // measure the amount of time it takes to fetch files, including time spent waiting
    val clockTimeInMillis = measureTimeMillis {
        people = fileUrls
            .map { fileUrl ->
                async {
                    try {
                        CsvMapperSingleton.getCsv(fileUrl)
                    } catch (ex: IOException) {
                        errorChannel.send(fileUrl.toString())
                        listOf()
                    }
                }
            }
            .awaitAll()
            .flatten()
            .toList()
    }
    // calling close() ensures all values that have been sent on this channel will be read before the receiver stops reading
    errorChannel.close()

    FetchFilesResult(
        people = people,
        clockTimeInMillis = clockTimeInMillis
    )
}

data class FetchFilesResult(
    val people: List<Person>,
    val clockTimeInMillis: Long,
)