import kotlinx.coroutines.async
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.runBlocking
import java.net.MalformedURLException
import java.net.URL

fun main(args: Array<String>) = try {
    runBlocking {
        val fileErrors = mutableListOf<String>()
        // mapNotNull skips nulls, this allows errors to be handled without later clean up of outputs.
        val fileUrls = args.mapNotNull { urlString ->
            try {
                URL(urlString)
            } catch (ex: MalformedURLException) {
                fileErrors.addFileError(urlString, "Malformed URL")
                null
            }
        }

        // Using a channel to send file errors from fetchFiles, guarding against mutating my fileErrors list concurrently
        val fileErrorChannel = Channel<String>()
        val deferredFileFetch = async {
            fetchFiles(fileUrls, fileErrorChannel)
        }
        // This iterates over a channel until a close signal is encountered
        for (errorFileUrl in fileErrorChannel) {
            fileErrors.addFileError(errorFileUrl, "Error fetching or reading the csv")
        }
        val (people, clockTimeInMillis) = deferredFileFetch.await()

        if (people.isEmpty()) {
            printReport(
                coroutineForFileFetchCount = fileUrls.size,
                clockTimeForFilesInMillis = clockTimeInMillis,
                fileErrors = fileErrors
            )
            return@runBlocking
        }

        val possiblyExpensiveValues = getPossiblyExpensiveValues(people)

        printReport(
            possiblyExpensiveValues = possiblyExpensiveValues,
            coroutineForFileFetchCount = fileUrls.size,
            clockTimeForFilesInMillis = clockTimeInMillis,
            fileErrors = fileErrors
        )
    }
} catch (exception: Exception) {
    println("Something unexpected went wrong, ${exception.message}")
}

fun printReport(
    possiblyExpensiveValues: PossiblyExpensiveValues? = null,
    coroutineForFileFetchCount: Int,
    clockTimeForFilesInMillis: Long,
    fileErrors: List<String>,
) {
    println("*****REPORT*****")
    println(
        if (possiblyExpensiveValues == null) {
            "No records found."
        } else {
            """
            |Average Age: ${possiblyExpensiveValues.average}
            |Median Age: ${possiblyExpensiveValues.median}
            |Name with Median Age: ${possiblyExpensiveValues.ageToNameMap[possiblyExpensiveValues.median] ?: "No name matches the median age"}
            """.trimMargin()
        }
    )
    println(
        """
        |Coroutines used to fetch and read files: ${coroutineForFileFetchCount + 1}
        |Clock time spent fetching and reading files in millis: $clockTimeForFilesInMillis
        |Files that were rejected:
        ${
            // This makes sure each line begins with | followed by the file with an error and a newline.
            // Starting with an empty string and only prepending | when an error file line is being created caused 
            // whitespace to be printed on a blank line (8 spaces in fact) which is a side effect of using trimMargin()
            fileErrors.fold("|") { message, fileError ->
                "$message$fileError\n|"
            }
        }
        """.trimMargin()
    )
}

fun MutableList<String>.addFileError(fileUrlString: String, reason: String) {
    add(fileUrlString)
    println(
        """
        |Adding file to error list.
        |   File: $fileUrlString
        |   Reason: $reason
        """.trimMargin()
    )
}

