import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope

suspend fun getPossiblyExpensiveValues(
    people: List<Person>
): PossiblyExpensiveValues = coroutineScope {
    val deferredAverage = async {
        people.map(Person::age).average()
    }

    val deferredMedian = async {
        getMedian(people.map(Person::age))
    }

    /**
     * This is a fairly simplistic way to get the name of an arbitrary person with a particular age.
     * In a production implementation there would likely be better ways to do this.
     */
    val deferredEasyAgeMap = async {
        people.associate { person ->
            person.age.toDouble() to "${person.firstName} ${person.lastName()}"
        }
    }

    PossiblyExpensiveValues(
        average = deferredAverage.await(),
        median = deferredMedian.await(),
        ageToNameMap = deferredEasyAgeMap.await()
    )
}

data class PossiblyExpensiveValues(
    val average: Double,
    val median: Double,
    val ageToNameMap: Map<Double, String>,
)

fun getMedian(ages: List<Int>): Double {
    require(ages.isNotEmpty()) {
        "getMedian does not support empty lists"
    }

    return if (ages.size == 1) {
        ages.first().toDouble()
    } else {
        val sortedAges = ages.sorted()
        val middleIndex = ages.size / 2

        if (ages.size % 2 != 0) {
            sortedAges[middleIndex].toDouble()
        } else {
            // sublist fromIndex is inclusive, but toIndex is exclusive, hence the + 1
            sortedAges.subList(middleIndex - 1, middleIndex + 1).average()
        }
    }
}