import com.fasterxml.jackson.dataformat.csv.CsvMapper
import com.fasterxml.jackson.dataformat.csv.CsvSchema
import com.fasterxml.jackson.module.kotlin.KotlinModule
import java.net.URL

object CsvMapperSingleton {
    private val csvMapper = CsvMapper().apply {
        registerModule(KotlinModule.Builder().build())
    }

    fun getCsv(url: URL) =
        csvMapper
            .readerFor(Person::class.java)
            .with(
                CsvSchema.builder()
                    .addColumn("fname", CsvSchema.ColumnType.STRING_OR_LITERAL)
                    .addColumn(" lname", CsvSchema.ColumnType.STRING_OR_LITERAL)
                    .addNumberColumn(" age")
                    .setUseHeader(true)
                    .setStrictHeaders(true)
                    .build()
            )
            .readValues<Person>(url)
            .readAll()
            .toList()
}