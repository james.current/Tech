import com.fasterxml.jackson.annotation.JsonAlias

// leading spaces in the lname and age aliases are making up for spaces in the headers
data class Person(
    @JsonAlias("fname")
    val firstName: String,
    @JsonAlias(" lname")
    val lname: String,
    @JsonAlias(" age")
    val age: Int
) {
    fun lastName(): String = lname.trim()
}