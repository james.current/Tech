# CrowdStrike Tech Interview Assignment

## How to run it
I used Kotlin with Gradle, so you'll need a JVM. I used (and target) JVM version 11, since this is an LTS version there
shouldn't be a problem with running it on a newer version (I tested it using Amazon Corretto 11.0.14.9.1 and 17.0.2.8.1)

All commands are given assuming your `pwd` is the project root and assuming you're using macOS or linux

### Tests
You can run my unit tests using `./gradlew test`

They don't have much output if they're successful, but use default output on one or more failures.

### Build
You can build this project using `./gradlew build`

The output directory is `build/distributions` inside will be a tar and a zip file whose contents are identical. Feel free
to extract the contents anywhere that is convenient as the build artifacts only require the contents of the archive to run.

### Run
Once extracted there is a script called `cs-tech-interview` in the `bin` directory from the archive file that runs the
project and takes commandline arguments directly.
```
build/distributions/cs-tech-interview-1.0-SNAPSHOT/bin/cs-tech-interview http://example.com/file1.csv http://example.com/file2.csv
```
Output will look something like this:
```
Adding file to error list.
   File: file:/Users/jamescurrent/Downloads/DetPlatCloudHomework/data/file6_bad.csv
   Reason: Error fetching or reading the csv
Adding file to error list.
   File: file:/Users/jamescurrent/Downloads/DetPlatCloudHomework/data/file9_bad.csv
   Reason: Error fetching or reading the csv
*****REPORT*****
Average Age: 33.80642857142857
Median Age: 31.0
Name with Median Age: Cassandra SHERMAN
Coroutines used to fetch and read files: 8
Clock time spent fetching and reading files in millis: 1047
Files that were rejected:
file:/Users/jamescurrent/Downloads/DetPlatCloudHomework/data/file6_bad.csv
file:/Users/jamescurrent/Downloads/DetPlatCloudHomework/data/file9_bad.csv
```

## Design

I chose Kotlin for two reasons. First, because I'm very comfortable with the language. Second, because it's coroutines are
much easier to deal with than Java's Threads, and (IIRC) are close to Golang's goroutines in nature. A lot of the logic
is in top level functions that only need the state passed to them, so no surrounding class was required. Kotlin follows
a pragmatic mix of OOP and Functional paradigms, since this dealt with data it's more functional than OOP, though if this
were targeted at a production environment more of the OOP would come out in database access and other service level patterns.

### Testing
The preferred testing framework in the Kotlin community is Kotest, so that's what I used (backed by JUnit5) to write my
unit tests. It allows unit tests to be fairly easy to read both with the test setup and the assertions DSL.

Along with unit testing the individual pieces I also ran manual tests to make sure that the whole thing actually ran properly,
to check the commandline display of error messages and the final report output, and to check that both file:/// urls and 
http:// urls worked.

### Rejected design
This primarily came out the way I envisioned it in my head. One thing that I changed was instead of passing a mutable list
in to the function that fetches files to record files that had issues and were discarded, I used a channel to ensure I didn't
run into concurrency problems with the mutable list.

### Assumptions
My first assumption was that all input files would fit in memory. Since this is a technical interview assignment that seemed
reasonable, along with the prompt that "The program is not expected to be ready for use at scale in production..." so I
controlled scope in that way to save time. Another assumption was that "clock time spent reading all the data files" meant
the time it took to read the files and deserialize them into objects. This would include idle time spent if all coroutines
were suspended and waiting for a remote server to respond. I made this choice primarily because of time constraints, I knew
how to do that, and while I likely could have figured out more granular timing, I wanted to spend my time elsewhere.

### 10M Records
Obviously, the first thing I'd need to change is the assumption about everything fitting in memory. Likely what would need
change is storing data in either a database or a distributed cache and use multiple nodes to process it. To be honest I
don't think Kotlin is necessarily well suited for large scale data analysis, so I'd likely choose a language with better
support for that, like python.

### 20K URLs
A similar solution to the previous question, it's unlikely that the data contained in 20k URLs would fit in memory on a
single node, so multiple nodes would be needed to process that many files, along with a db or distributed cache to put all
the information in for further analysis after reading it in. Specific to dealing with 20K URLs, I'd likely use event driven
compute nodes, whether those were AWS lambdas (or equivalent) or a docker cluster with long-running nodes is something that
would depend on current patterns in the platform I was implementing it in.

### Testing for production use at scale
Obviously I'd still have unit tests to make sure that each individual piece works as intended. But testing that it will
run at scale at the very least means writing and running load tests in a prod-like environment, and possibly running performance
tests if speed is a concern.